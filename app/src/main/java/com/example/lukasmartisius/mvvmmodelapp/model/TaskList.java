package com.example.lukasmartisius.mvvmmodelapp.model;

import java.util.ArrayList;
import java.util.List;


public class TaskList {
        ArrayList<Task> taskList=new ArrayList<Task>();

        public void addTask(Task newTask)
        {
            taskList.add(newTask);
        }

        public void removeTask(int id)
        {
            /*for (Task t: taskList) {
                if(t.getId()==id)
                    taskList.remove(t);
            }*/
            taskList.remove(id);
        }

        public Task getTask(int id)
        {
            /*for (Task t: taskList) {
                if(t.getId()==id)
                    return t;
            }*/
            return taskList.get(id);
        }

        public List<Task> getTaskList()
        {
            return taskList;
        }

        public int getTaskListLength()
        {
            return taskList.size();
        }


}
